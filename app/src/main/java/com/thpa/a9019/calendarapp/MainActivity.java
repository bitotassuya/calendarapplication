package com.thpa.a9019.calendarapp;

import android.app.DatePickerDialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.kd.dynamic.calendar.generator.ImageGenerator;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText mDataEditText;
    Calendar mCurrentData;
    Bitmap mGeneratedDataIcon;
    ImageGenerator mImageGenerator;
    ImageView mDisplayGeneratedImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageGenerator = new ImageGenerator(this);
        mDataEditText = (EditText) findViewById(R.id.txtDateEntered);
        mDisplayGeneratedImage = (ImageView) findViewById(R.id.imageGen);

        mImageGenerator.setIconSize(250, 250);
        mImageGenerator.setDateSize(100);
        mImageGenerator.setMonthSize(50);

        mImageGenerator.setDatePosition(160);
        mImageGenerator.setMonthPosition(40);

        mImageGenerator.setDateColor(Color.parseColor("#3c6eaf"));
        mImageGenerator.setMonthColor(Color.WHITE);

        mImageGenerator.setStorageToSDCard(true);

        mDataEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentData = Calendar.getInstance();
                int year = mCurrentData.get(Calendar.YEAR);
                int month = mCurrentData.get(Calendar.MONTH);
                int day = mCurrentData.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

                        mDataEditText.setText(selectedDay + "_" + selectedMonth + "_" + selectedYear);

                        mCurrentData.set(selectedYear, selectedMonth, selectedDay);
                        mGeneratedDataIcon = mImageGenerator.generateDateImage(mCurrentData,R.drawable.calendar);
                        mDisplayGeneratedImage.setImageBitmap(mGeneratedDataIcon);
                    }
                    },year,month,day);
                mDatePicker.show();


            }
        });
    }}
